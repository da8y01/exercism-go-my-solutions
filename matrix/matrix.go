package matrix

import (
	"errors"
	"strconv"
	"strings"
)

type Matrix struct {
	rows [][]int
	cols [][]int
}

func New(inp string) (*Matrix, error) {
	firstSpl := strings.Split(inp, "\n")
	out := new(Matrix)
	var err error
	var maxJ int
	for i := 0; err == nil && i < len(firstSpl); i++ {
		row := strings.Split(strings.TrimSpace(firstSpl[i]), " ")
		if len(row) == 0 {
			err = errors.New("Empty row")
		}
		if i == 0 {
			maxJ = len(row)
		}
		if maxJ != len(row) {
			err = errors.New("Uneven rows")
		}
		intRow := []int{}
		for j := 0; err == nil && j < len(row); j++ {
			var el int
			if el, err = strconv.Atoi(row[j]); err == nil {
				intRow = append(intRow, el)
			}
		}
		if err == nil {
			out.rows = append(out.rows, intRow)
		}
	}
	if err != nil {
		return nil, err
	}
	if len(out.rows) > 0 {
		for j := 0; j < len(out.rows[0]); j++ {
			intCol := []int{}
			for i := 0; i < len(out.rows); i++ {
				intCol = append(intCol, out.rows[i][j])
			}
			out.cols = append(out.cols, intCol)
		}
	}
	return out, err
}

func (m Matrix) Rows() [][]int {
	out := [][]int{}
	for i := 0; i < len(m.rows); i++ {
		row := make([]int, len(m.rows[i]))
		copy(row, m.rows[i])
		out = append(out, row)
	}
	return out
}

func (m Matrix) Cols() [][]int {
	out := [][]int{}
	for i := 0; i < len(m.cols); i++ {
		col := make([]int, len(m.cols[i]))
		copy(col, m.cols[i])
		out = append(out, col)
	}
	return out
}

func (m *Matrix) Set(i, j, val int) bool {
	if i < 0 || j < 0 || i >= len(m.rows) || j >= len(m.cols) {
		return false
	}
	m.rows[i][j] = val
	m.cols[j][i] = val
	return true
}
