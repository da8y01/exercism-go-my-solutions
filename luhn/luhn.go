package luhn

import (
	"regexp"
)

func Valid(inp string) bool {
	re := regexp.MustCompile("[^0-9 ]")
	snum := re.ReplaceAllString(inp, "")
	if inp != snum {
		return false
	}
	re = regexp.MustCompile(" ")
	snum = re.ReplaceAllString(snum, "")

	if len(snum) < 2 {
		return false
	}

	j := len(snum) - 1
	top := len(snum) - 1
	total := 0
	for i := 0; i < len(snum); i++ {
		if (top - j) % 2 == 1 {
            total += (num(snum[j]) * 2) % 9
		} else {
            total += num(snum[j])
		}
		j--
	}
	return (total % 10) == 0
}

func num(c byte) int {
	var zero byte
	zero = '0'
	return int(c - zero)
}
