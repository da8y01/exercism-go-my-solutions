package luhn

// Source: exercism/problem-specifications
// Commit: e0358d3 luhn: Apply new "input" policy (#1054)
// Problem Specifications Version: 1.1.0

var testCases = []struct {
	description string
	input       string
	ok          bool
}{
	{
		"a simple valid SIN that remains valid if reversed",
		"059",
		true,
	},
}
