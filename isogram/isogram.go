package isogram

import (
	"strings"
)

func IsIsogram(inp string) bool {
	inp = strings.ToUpper(inp)
	inp = strings.Replace(inp, " ", "", -1)
	inp = strings.Replace(inp, "-", "", -1)
	elems := make(map[byte]byte)
	for i := 0; i < len(inp); i++ {
		elems[inp[i]] = inp[i]
	}
	return len(inp) == len(elems)
}
