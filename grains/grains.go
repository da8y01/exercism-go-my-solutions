package grains

import (
	"errors"
)

func Square(num int) (uint64, error) {
	if num > 64 || num < 1 {
		return 0, errors.New("Invalid number.")
	}
	out := pow2(num)
	return out, nil
}

func Total() uint64 {
	return pow2(65) - 1
}

func pow2(num int) uint64 {
	var out uint64 = 1
	return out << uint(num-1)
}
