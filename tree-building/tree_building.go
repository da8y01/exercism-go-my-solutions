package tree

import (
	"errors"
	"sort"
)

type Record struct {
	ID, Parent int
}

type Node struct {
	ID       int
	Children []*Node
}

type Mismatch struct{}

func (m Mismatch) Error() string {
	return "c"
}

func Build(records []Record) (*Node, error) {
	if len(records) == 0 {
		return nil, nil
	}
	if !checkRecords(records) {
		return nil, errors.New("a")
	}
	root := fixNode(&Node{
		ID: 0,
		Children: findChilds(0, records),
	})
	return root, nil
}

func findChilds(id int, records []Record) []*Node {
	out := make([]*Node, 0, len(records))
	j := 0
	for i := 0; i < len(records); i++ {
		if records[i].ID != 0 && records[i].Parent == id {
			nod := fixNode(&Node{
				ID: records[i].ID,
				Children: findChilds(records[i].ID, records),
			})
			out = append(out, nod)
			j++
		}
	}
	sort.Slice(out, func (i, j int) bool {
		return out[i].ID < out[j].ID
	})
	return out
}

func checkRecords(records []Record) bool {
	out := true
	foundRoot := false
	m := make(map[int]int)
	max := -1
	for i := 0; out && i < len(records); i++ {
		out = records[i].ID == 0 && records[i].Parent == 0 || records[i].ID > records[i].Parent
		if records[i].ID == 0 {
			foundRoot = true
		}
		m[records[i].ID] = records[i].ID
		if records[i].ID > max {
			max = records[i].ID
		}
	}
	if !out || !foundRoot || len(records) != len(m) || max >= len(records) {
		return false
	}
	return true
}

func fixNode(inp *Node) *Node {
	if len(inp.Children) == 0 {
		return &Node{ ID: inp.ID }
	}
	return inp
}
