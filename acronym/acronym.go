package acronym

import "strings"

func Abbreviate(s string) string {
	spl := strings.Split(strings.Replace(s, "-", " ", -1), " ")
	for i := 0; i < len(spl); i++ {
		spl[i] = string(spl[i][0])
	}
	return strings.ToUpper(strings.Join(spl, ""))
}
