package robotname

import (
	"time"
	"math/rand"
)

type Robot struct {
	N string
}

var rnd = rand.New(rand.NewSource(time.Now().UnixNano()))
var names = make([]string, 0, 200)

func (r *Robot) Name() string  {
	if r.N == "" {
		r.Reset()
	}
	return r.N
}

func (r *Robot) Reset() {
	name := randomLetter() + randomLetter() + randomNumber() + randomNumber() + randomNumber()
	for ; found(name); {
		name = randomLetter() + randomLetter() + randomNumber() + randomNumber() + randomNumber()
	}
	r.N = name
	names = append(names, r.N)
}

func found(item string) bool {
	out := false
	for i := 0; !out && i < len(names); i++ {
		out = names[i] == item
	}
	return out
}

func randomLetter() string {
	base := 'A'
	max := 26 // to Z
	l := int(base) + rnd.Intn(max)
	return string(l)
}

func randomNumber() string {
  return string(int('0') + rnd.Intn(10))
}
