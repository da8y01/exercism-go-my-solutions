package clock

type Clock struct {
	Hours int
	Minutes int
}

func New(hours, minutes int) Clock {
	h := mod(hours + floor(float64(minutes) / 60), 24)
	c := Clock{
		Hours: h,
		Minutes: mod(minutes, 60),
	}
	return c
}

func (c Clock) String() string {
	return num2dig(c.Hours) + ":" + num2dig(c.Minutes)
}

func (c Clock) Add(minutes int) Clock {
	return New(c.Hours, c.Minutes + minutes)
}

func (c Clock) Subtract(minutes int) Clock {
	return New(c.Hours, c.Minutes - minutes)
}

func mod(n, d int) int {
	n %= d
	if n < 0 {
		n += d
	}
	return n
}

func floor(inp float64) int {
	if inp < 0 && float64(int(inp)) != inp {
		return int(inp - 1)
	}
	return int(inp)
}

func num2dig(inp int) string {
	if inp < 10 {
		return "0" + string('0' + inp)
	}
	c0 := inp % 10
	c1 := inp / 10
	return string('0' + c1) + string('0' + c0)
}
