package twelve

import (
	"fmt"
	"strings"
)

const FIRST = "On the %s day of Christmas my true love gave to me"

var POSITIONS = []string{
	"first",
	"second",
	"third",
	"fourth",
	"fifth",
	"sixth",
	"seventh",
	"eighth",
	"ninth",
	"tenth",
	"eleventh",
	"twelfth",
}

var PHRASES = []string{
	"twelve Drummers Drumming",
	"eleven Pipers Piping",
	"ten Lords-a-Leaping",
	"nine Ladies Dancing",
	"eight Maids-a-Milking",
	"seven Swans-a-Swimming",
	"six Geese-a-Laying",
	"five Gold Rings",
	"four Calling Birds",
	"three French Hens",
	"two Turtle Doves",
	"and a Partridge in a Pear Tree.",
}

func Song() string {
	var b strings.Builder
	for i := 1; i <= 12; i++ {
		fmt.Fprintf(&b, "%s\n", Verse(i))
	}
	return b.String()
}

func Verse(inp int) string {
	if inp < 1 {
		inp = 1
	}
	if inp > 12 {
		inp = 12
	}
	out := make([]string, inp+1)
	for i := 0; i < inp; i++ {
		phrase := PHRASES[len(PHRASES)-1-i]
		if inp == 1 {
			runes := []rune(phrase)
			phrase = string(runes[4:len(runes)])
		}
		out[len(out)-1-i] = phrase
	}
	out[0] = fmt.Sprintf(FIRST, POSITIONS[inp-1])
	return strings.Join(out, ", ")
}
