package tournament

import (
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"sort"
	"strings"
)

type Team struct {
	MP, W, D, L, P int
	Name           string
}

func Tally(r io.Reader, w io.Writer) error {
	b, _ := ioutil.ReadAll(r)
	lines := strings.Split(string(b), "\n")
	teams := make(map[string]*Team)
	var err error
	var result []string
	for i := 0; err == nil && i < len(lines); i++ {
		result, err = readLine(lines[i])
		if err == nil && result != nil {
			if _, ok := teams[result[0]]; !ok {
				teams[result[0]] = new(Team)
				teams[result[0]].Name = result[0]
			}
			if _, ok := teams[result[1]]; !ok {
				teams[result[1]] = new(Team)
				teams[result[1]].Name = result[1]
			}
			teams[result[0]].MP++
			teams[result[1]].MP++
			switch result[2] {
			case "win":
				teams[result[0]].W++
				teams[result[1]].L++
				teams[result[0]].P += 3
			case "loss":
				teams[result[0]].L++
				teams[result[1]].W++
				teams[result[1]].P += 3
			case "draw":
				teams[result[0]].D++
				teams[result[1]].D++
				teams[result[0]].P++
				teams[result[1]].P++
			}
		}
	}
	if err != nil {
		return err
	}
	t2 := make([]*Team, len(teams))
	i := 0
	for _, v := range teams {
		t2[i] = v
		i++
	}
	sort.Slice(t2, func(i, j int) bool {
		if t2[i].P > t2[j].P {
			return true
		}
		if t2[i].P < t2[j].P {
			return false
		}
		return t2[i].Name < t2[j].Name
	})
	io.WriteString(w, "Team                           | MP |  W |  D |  L |  P\n")
	for i := 0; i < len(t2); i++ {
		v := t2[i]
		k := v.Name
		io.WriteString(w, fmt.Sprintf("%-30s | %s | %s | %s | %s | %s\n", k, to2str(v.MP), to2str(v.W), to2str(v.D), to2str(v.L), to2str(v.P)))
	}
	return nil
}

func to2str(inp int) string {
	if inp < 10 {
		return " " + string('0'+inp)
	}
	return string('0'+inp/10) + string('0'+inp%10)
}

func readLine(inp string) ([]string, error) {
	if inp == "" || inp[0] == '#' {
		return nil, nil
	}
	out := strings.Split(inp, ";")
	if len(out) != 3 {
		return nil, errors.New("Bad format line")
	}
	if out[0] == "" || out[1] == "" {
		return nil, errors.New("Empty team(s) name")
	}
	if out[2] != "win" && out[2] != "loss" && out[2] != "draw" {
		return nil, errors.New("Game status is not win/loss/draw")
	}
	return out, nil
}
